const fetch = require('isomorphic-unfetch')

module.exports = async (req, res) => {
  const webhookUrl = process.env.SIGNUP_WEBHOOK_URL;

  if (!req.body) {
    return res.status(400).json(JSON.stringify({"error": "Request body is required"}))
  }

  let body = {};
  if (typeof req.body === 'string') {
    try {
      body = JSON.parse(req.body);
    } catch(e) {}
  } else if (typeof req.body === 'object') {
    body = req.body
  }

  const webhookResponse = await fetch(webhookUrl, {
    method: 'POST',
    body: JSON.stringify({
      text: `Signup from ${body.email}`,
    }),
  });

  res.status(200).json(webhookResponse);
};
